﻿using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FileScanner.Common;

namespace FileScanner.TextPlugin
{
    public class TextPluginImpl : PluginBase
    {
        private PluginOptions _options;
        // TODO в настройки
        private readonly int _chunkSize = 1000;

        public override IPluginOptions Options => _options;
        public override string Name => "Поиск по вхождению строки в текстовый файл (*.txt)";
        
        public TextPluginImpl()
        {
            _options = new PluginOptions();
        }

        public override bool CheckFile(string fileName, CancellationToken token)
        {
            using (FileStream fs = File.OpenRead(fileName))
            {
                byte[] array = new byte[_chunkSize];
                while (true)
                {
                    if (token.IsCancellationRequested)
                        break;

                    var factSize = fs.Read(array, 0, _chunkSize);
                    string text = Encoding.Default.GetString(array);
                    if (text.Contains(_options.TextPattern))
                        return true;
                    if (factSize < _chunkSize)
                        break;

                    // нахлест при чтении необходим, чтобы не пропустить вхождения строки на стыке кусков
                    fs.Seek(-_options.TextPattern.Length, SeekOrigin.Current);
                }
                return false;
            }
        }

        public override Form CreateOptionsWindow()
        {
            return new PluginForm(Name, _options);
        }
    }
}
