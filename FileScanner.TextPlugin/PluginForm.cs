﻿using System.Windows.Forms;

namespace FileScanner.TextPlugin
{
    internal partial class PluginForm : Form
    {
        private PluginOptions _options;

        private TextBox textBoxPattern;
        private Label labelPattern;
        private Button buttonCancel;
        private Button buttonOk;

        private System.ComponentModel.IContainer components = null;

        public PluginForm(string formName, PluginOptions options)
        {
            InitializeComponent();

            _options = options;

            this.Text = formName;
            textBoxPattern.Text = _options.TextPattern;
        }

        private void InitializeComponent()
        {
            this.textBoxPattern = new System.Windows.Forms.TextBox();
            this.labelPattern = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxPattern
            // 
            this.textBoxPattern.Location = new System.Drawing.Point(226, 48);
            this.textBoxPattern.Name = "textBoxPattern";
            this.textBoxPattern.Size = new System.Drawing.Size(389, 26);
            this.textBoxPattern.TabIndex = 0;
            // 
            // labelPattern
            // 
            this.labelPattern.AutoSize = true;
            this.labelPattern.Location = new System.Drawing.Point(33, 51);
            this.labelPattern.Name = "labelPattern";
            this.labelPattern.Size = new System.Drawing.Size(187, 20);
            this.labelPattern.TabIndex = 1;
            this.labelPattern.Text = "Подстрока для поиска:";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(525, 141);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(113, 43);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "ОК";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(392, 141);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(113, 43);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // PluginForm
            // 
            this.ClientSize = new System.Drawing.Size(686, 220);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelPattern);
            this.Controls.Add(this.textBoxPattern);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "PluginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, System.EventArgs e)
        {
            _options.TextPattern = textBoxPattern.Text;
            this.Close();
        }
    }
}
