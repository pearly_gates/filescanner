﻿using FileScanner.Common;
using System;

namespace FileScanner.TextPlugin
{
    internal class PluginOptions :IPluginOptions
    {
        public string TextPattern { get; set; }
        public bool IsEmpty => String.IsNullOrEmpty(TextPattern);

        public string AsString()
        {
            var pattern = String.IsNullOrEmpty(TextPattern) ? "пусто" : TextPattern;
            return $"Строка: <{pattern}>";
        }
    }
}