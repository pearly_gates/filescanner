﻿using System.Windows.Forms;

namespace FileScanner.Mp3Plugin
{
    internal partial class PluginForm : Form
    {
        private PluginOptions _options;

        private Label labelArtist;
        private Label labelTitle;
        private TextBox textBoxArtist;
        private TextBox textBoxTitle;
        private Button buttonCancel;
        private Button buttonOk;
        private System.ComponentModel.IContainer components = null;

        public PluginForm(string formName, PluginOptions options)
        {
            InitializeComponent();

            _options = options;

            this.Text = formName;
            textBoxArtist.Text = _options.Artist;
            textBoxTitle.Text = _options.Title;
        }

        private void InitializeComponent()
        {
            this.textBoxArtist = new System.Windows.Forms.TextBox();
            this.labelArtist = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxArtist
            // 
            this.textBoxArtist.Location = new System.Drawing.Point(176, 33);
            this.textBoxArtist.Name = "textBoxArtist";
            this.textBoxArtist.Size = new System.Drawing.Size(389, 26);
            this.textBoxArtist.TabIndex = 0;
            // 
            // labelArtist
            // 
            this.labelArtist.AutoSize = true;
            this.labelArtist.Location = new System.Drawing.Point(55, 36);
            this.labelArtist.Name = "labelArtist";
            this.labelArtist.Size = new System.Drawing.Size(111, 20);
            this.labelArtist.TabIndex = 1;
            this.labelArtist.Text = "Исполнитель";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(525, 141);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(113, 43);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "ОК";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(392, 141);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(113, 43);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(35, 79);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(131, 20);
            this.labelTitle.TabIndex = 5;
            this.labelTitle.Text = "Название песни";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(176, 76);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(389, 26);
            this.textBoxTitle.TabIndex = 4;
            // 
            // PluginForm
            // 
            this.ClientSize = new System.Drawing.Size(686, 220);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelArtist);
            this.Controls.Add(this.textBoxArtist);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "PluginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, System.EventArgs e)
        {
            _options.Artist = textBoxArtist.Text;
            _options.Title = textBoxTitle.Text;
            this.Close();
        }
    }
}
