﻿using FileScanner.Common;
using System;

namespace FileScanner.Mp3Plugin
{
    internal class PluginOptions :IPluginOptions
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public bool IsEmpty => String.IsNullOrEmpty(Artist) && String.IsNullOrEmpty(Title);

        public string AsString()
        {
            return $"Исполнитель: <{Artist}>; Песня: <{Title}>";
        }
    }
}