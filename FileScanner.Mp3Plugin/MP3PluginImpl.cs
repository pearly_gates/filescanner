﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using FileScanner.Common;
using Id3;

namespace FileScanner.Mp3Plugin
{
    public class MP3PluginImpl : PluginBase
    {
        private PluginOptions _options;

        public override IPluginOptions Options => _options;
        public override string Name => "Поиск по названию песни и исполнителю (*.mp3)";

        public MP3PluginImpl()
        {
            _options = new PluginOptions();
        }

        public override bool CheckFile(string fileName, CancellationToken token)
        {
            using (var mp3 = new Mp3(fileName))
            {
                if (!mp3.HasTags)
                    return false;

                try
                {
                    var tagV1 = mp3.GetTag(Id3TagFamily.Version1X);
                    var tagV2 = mp3.GetTag(Id3TagFamily.Version2X);

                    return CheckByID3Tag(tagV1) || CheckByID3Tag(tagV2);
                }
                catch (Exception)
                {
                    // TODO запись в журнал
                    return false;
                }
            }
        }

        public override Form CreateOptionsWindow()
        {
            return new PluginForm(Name, _options);
        }

        private bool CheckByID3Tag(Id3Tag tag)
        {
            if (tag == null)
                return false;
            
            // TODO берем только первого исполнителя
            var artist = tag.Artists.Value.FirstOrDefault();
            var title = tag.Title.Value;

            return CheckByArtistAndTitle(artist, title);
        }

        private bool CheckByArtistAndTitle(string artist, string title)
        {
            // TODO расширить до case insensitive + проверять вхождение строки
            if ((artist != _options.Artist) && !String.IsNullOrEmpty(_options.Artist))
                return false;

            if ((title != _options.Title) && !String.IsNullOrEmpty(_options.Title))
                return false;

            return true;
        }
    }
}
