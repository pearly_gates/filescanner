﻿using FileScanner.Common;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace FileScanner
{
    public partial class MainForm : Form
    {
        // TODO в конфиг
        private readonly string _pluginFolder = @"\Plugins";

        private bool isSearching;
        private FileSearchEngine searchEngine = null;

        private string _pluginPath => $"{Environment.CurrentDirectory}{_pluginFolder}";
        private IPlugin _plugin => comboBoxPlugins.SelectedItem as IPlugin;
        private string _targetFolder => textBoxFolder.Text;
        private bool _includeSubfolders => checkBoxSubfolders.Checked;

        public MainForm()
        {
            InitializeComponent();
        }

        private void LoadPlugins()
        {
            var pluginList = PluginHelper.GetAvailablePlugins(_pluginPath);

            comboBoxPlugins.Items.Clear();
            comboBoxPlugins.Items.AddRange(pluginList.ToArray());

            buttonPluginOptions.Enabled = false;

            labelPluginInfo.Text = "Загружены новые плагины";
            labelPluginInfo.ForeColor = Color.Teal;
        }

        private void OpenPluginWindow(IPlugin plugin)
        {
            if (plugin == null)
                return;

            using (var form = plugin.CreateOptionsWindow())
            {
                form.Owner = this;
                form.ShowDialog();
            }

            RefreshPluginOptions();
        }

        private void RefreshPluginOptions()
        {
            labelPluginInfo.Text = _plugin?.Options.AsString();
            labelPluginInfo.ForeColor = Color.Black;
        }

        private void StartSearch()
        {
            searchEngine = new FileSearchEngine(_targetFolder, _includeSubfolders, _plugin);
            searchEngine.Start(
            file =>
            {
                this.BeginInvoke(new Action<string>((f) => listBoxResult.Items.Insert(0, f)), file);
            },
            t =>
            {
                this.BeginInvoke(new Action(() => SetSearchState(false)));
            });
        }

        private void SetSearchState(bool isActive)
        {
            isSearching = isActive;

            if (isActive)
                listBoxResult.Items.Clear();

            pictureBoxProcessing.Visible = isActive;
            buttonStartStop.Text = isActive ? "Стоп" : "Старт";
            textBoxFolder.Enabled = !isActive;
            comboBoxPlugins.Enabled = !isActive;
            buttonPluginOptions.Enabled = !isActive;
            checkBoxSubfolders.Enabled = !isActive;
        }

        private void ShowWarning(string text)
        {
            MessageBox.Show(text, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonSelectFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                textBoxFolder.Text = folderBrowserDialog.SelectedPath;
        }

        private void buttonPluginOptions_Click(object sender, EventArgs e)
        {
            OpenPluginWindow(_plugin);
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            if (isSearching)
            {
                searchEngine.Stop();
                SetSearchState(false);
                return;
            }

            if (!Directory.Exists(_targetFolder))
            {
                ShowWarning("Выбран несуществующий каталог");
                return;
            }
            if (_plugin == null)
            {
                ShowWarning("Не выбран плагин для поиска файлов");
                return;
            }
            if (_plugin.Options.IsEmpty)
            {
                ShowWarning("Не заданы параметры поиска в плагине");
                return;
            }

            SetSearchState(true);
            StartSearch();
        }

        private void fileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            LoadPlugins();
        }

        private void comboBoxPlugins_DropDown(object sender, EventArgs e)
        {
            labelPluginInfo.Text = String.Empty;
        }

        private void comboBoxPlugins_SelectedValueChanged(object sender, EventArgs e)
        {
            buttonPluginOptions.Enabled = true;
            RefreshPluginOptions();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                fileSystemWatcher.Path = _pluginPath;
            }
            catch (ArgumentException)
            {
                MessageBox.Show($"Отсутствует папка с плагинами - {_pluginFolder}", "FileScanner - Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            LoadPlugins();
            SetSearchState(false);
        }
    }
}
