﻿namespace FileScanner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.fileSystemWatcher = new System.IO.FileSystemWatcher();
            this.panelOptions = new System.Windows.Forms.Panel();
            this.pictureBoxProcessing = new System.Windows.Forms.PictureBox();
            this.labelPluginInfo = new System.Windows.Forms.Label();
            this.buttonSelectFolder = new System.Windows.Forms.Button();
            this.buttonPluginOptions = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelPlugin = new System.Windows.Forms.Label();
            this.checkBoxSubfolders = new System.Windows.Forms.CheckBox();
            this.buttonStartStop = new System.Windows.Forms.Button();
            this.comboBoxPlugins = new System.Windows.Forms.ComboBox();
            this.textBoxFolder = new System.Windows.Forms.TextBox();
            this.labelFolder = new System.Windows.Forms.Label();
            this.panelResult = new System.Windows.Forms.Panel();
            this.listBoxResult = new System.Windows.Forms.ListBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).BeginInit();
            this.panelOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcessing)).BeginInit();
            this.panelResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // fileSystemWatcher
            // 
            this.fileSystemWatcher.EnableRaisingEvents = true;
            this.fileSystemWatcher.SynchronizingObject = this;
            this.fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher_Changed);
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.pictureBoxProcessing);
            this.panelOptions.Controls.Add(this.labelPluginInfo);
            this.panelOptions.Controls.Add(this.buttonSelectFolder);
            this.panelOptions.Controls.Add(this.buttonPluginOptions);
            this.panelOptions.Controls.Add(this.labelResult);
            this.panelOptions.Controls.Add(this.labelPlugin);
            this.panelOptions.Controls.Add(this.checkBoxSubfolders);
            this.panelOptions.Controls.Add(this.buttonStartStop);
            this.panelOptions.Controls.Add(this.comboBoxPlugins);
            this.panelOptions.Controls.Add(this.textBoxFolder);
            this.panelOptions.Controls.Add(this.labelFolder);
            this.panelOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelOptions.Location = new System.Drawing.Point(0, 0);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(979, 257);
            this.panelOptions.TabIndex = 9;
            // 
            // pictureBoxProcessing
            // 
            this.pictureBoxProcessing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxProcessing.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcessing.Image")));
            this.pictureBoxProcessing.Location = new System.Drawing.Point(717, 186);
            this.pictureBoxProcessing.Name = "pictureBoxProcessing";
            this.pictureBoxProcessing.Size = new System.Drawing.Size(61, 56);
            this.pictureBoxProcessing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxProcessing.TabIndex = 19;
            this.pictureBoxProcessing.TabStop = false;
            // 
            // labelPluginInfo
            // 
            this.labelPluginInfo.ForeColor = System.Drawing.Color.Black;
            this.labelPluginInfo.Location = new System.Drawing.Point(210, 141);
            this.labelPluginInfo.Name = "labelPluginInfo";
            this.labelPluginInfo.Size = new System.Drawing.Size(689, 42);
            this.labelPluginInfo.TabIndex = 18;
            this.labelPluginInfo.Text = "labelPluginInfo";
            // 
            // buttonSelectFolder
            // 
            this.buttonSelectFolder.Location = new System.Drawing.Point(918, 24);
            this.buttonSelectFolder.Name = "buttonSelectFolder";
            this.buttonSelectFolder.Size = new System.Drawing.Size(36, 36);
            this.buttonSelectFolder.TabIndex = 17;
            this.buttonSelectFolder.Text = "...";
            this.buttonSelectFolder.UseVisualStyleBackColor = true;
            this.buttonSelectFolder.Click += new System.EventHandler(this.buttonSelectFolder_Click);
            // 
            // buttonPluginOptions
            // 
            this.buttonPluginOptions.Location = new System.Drawing.Point(918, 101);
            this.buttonPluginOptions.Name = "buttonPluginOptions";
            this.buttonPluginOptions.Size = new System.Drawing.Size(36, 36);
            this.buttonPluginOptions.TabIndex = 16;
            this.buttonPluginOptions.Text = "...";
            this.buttonPluginOptions.UseVisualStyleBackColor = true;
            this.buttonPluginOptions.Click += new System.EventHandler(this.buttonPluginOptions_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(12, 222);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(156, 20);
            this.labelResult.TabIndex = 15;
            this.labelResult.Text = "Результаты поиска";
            // 
            // labelPlugin
            // 
            this.labelPlugin.AutoSize = true;
            this.labelPlugin.Location = new System.Drawing.Point(45, 108);
            this.labelPlugin.Name = "labelPlugin";
            this.labelPlugin.Size = new System.Drawing.Size(159, 20);
            this.labelPlugin.TabIndex = 14;
            this.labelPlugin.Text = "Плагин для поиска:";
            // 
            // checkBoxSubfolders
            // 
            this.checkBoxSubfolders.AutoSize = true;
            this.checkBoxSubfolders.Location = new System.Drawing.Point(214, 61);
            this.checkBoxSubfolders.Name = "checkBoxSubfolders";
            this.checkBoxSubfolders.Size = new System.Drawing.Size(265, 24);
            this.checkBoxSubfolders.TabIndex = 13;
            this.checkBoxSubfolders.Text = "включая вложенные каталоги";
            this.checkBoxSubfolders.UseVisualStyleBackColor = true;
            // 
            // buttonStartStop
            // 
            this.buttonStartStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStartStop.Location = new System.Drawing.Point(795, 195);
            this.buttonStartStop.Name = "buttonStartStop";
            this.buttonStartStop.Size = new System.Drawing.Size(159, 36);
            this.buttonStartStop.TabIndex = 12;
            this.buttonStartStop.Text = "buttonStartStop";
            this.buttonStartStop.UseVisualStyleBackColor = true;
            this.buttonStartStop.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // comboBoxPlugins
            // 
            this.comboBoxPlugins.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.comboBoxPlugins.DisplayMember = "Name";
            this.comboBoxPlugins.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPlugins.FormattingEnabled = true;
            this.comboBoxPlugins.Location = new System.Drawing.Point(214, 105);
            this.comboBoxPlugins.Name = "comboBoxPlugins";
            this.comboBoxPlugins.Size = new System.Drawing.Size(685, 28);
            this.comboBoxPlugins.TabIndex = 11;
            this.comboBoxPlugins.ValueMember = "Name";
            this.comboBoxPlugins.DropDown += new System.EventHandler(this.comboBoxPlugins_DropDown);
            this.comboBoxPlugins.SelectedValueChanged += new System.EventHandler(this.comboBoxPlugins_SelectedValueChanged);
            // 
            // textBoxFolder
            // 
            this.textBoxFolder.Location = new System.Drawing.Point(214, 29);
            this.textBoxFolder.Name = "textBoxFolder";
            this.textBoxFolder.Size = new System.Drawing.Size(685, 26);
            this.textBoxFolder.TabIndex = 10;
            // 
            // labelFolder
            // 
            this.labelFolder.AutoSize = true;
            this.labelFolder.Location = new System.Drawing.Point(38, 32);
            this.labelFolder.Name = "labelFolder";
            this.labelFolder.Size = new System.Drawing.Size(166, 20);
            this.labelFolder.TabIndex = 9;
            this.labelFolder.Text = "Каталог для поиска:";
            // 
            // panelResult
            // 
            this.panelResult.Controls.Add(this.listBoxResult);
            this.panelResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResult.Location = new System.Drawing.Point(0, 257);
            this.panelResult.Name = "panelResult";
            this.panelResult.Size = new System.Drawing.Size(979, 370);
            this.panelResult.TabIndex = 10;
            // 
            // listBoxResult
            // 
            this.listBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxResult.FormattingEnabled = true;
            this.listBoxResult.ItemHeight = 20;
            this.listBoxResult.Location = new System.Drawing.Point(12, 12);
            this.listBoxResult.Name = "listBoxResult";
            this.listBoxResult.Size = new System.Drawing.Size(955, 344);
            this.listBoxResult.TabIndex = 0;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 627);
            this.Controls.Add(this.panelResult);
            this.Controls.Add(this.panelOptions);
            this.MinimumSize = new System.Drawing.Size(1001, 683);
            this.Name = "MainForm";
            this.Text = "FileScanner";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).EndInit();
            this.panelOptions.ResumeLayout(false);
            this.panelOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcessing)).EndInit();
            this.panelResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.IO.FileSystemWatcher fileSystemWatcher;
        private System.Windows.Forms.Panel panelResult;
        private System.Windows.Forms.Panel panelOptions;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelPlugin;
        private System.Windows.Forms.CheckBox checkBoxSubfolders;
        private System.Windows.Forms.Button buttonStartStop;
        private System.Windows.Forms.ComboBox comboBoxPlugins;
        private System.Windows.Forms.TextBox textBoxFolder;
        private System.Windows.Forms.Label labelFolder;
        private System.Windows.Forms.Button buttonPluginOptions;
        private System.Windows.Forms.Button buttonSelectFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ListBox listBoxResult;
        private System.Windows.Forms.Label labelPluginInfo;
        private System.Windows.Forms.PictureBox pictureBoxProcessing;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}

