﻿using System.Threading;
using System.Windows.Forms;

namespace FileScanner.Common
{
    public interface IPlugin
    {
        string Name { get; }
        IPluginOptions Options { get; }

        Form CreateOptionsWindow();
        bool CheckFile(string file, CancellationToken token);
    }
}
