﻿namespace FileScanner.Common
{
    public interface IPluginOptions
    {
        bool IsEmpty { get; }
        string AsString();
    }
}