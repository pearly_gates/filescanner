﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FileScanner.Common
{
    public static class PluginHelper
    {
        private const string PluginInterfaceName = "IPlugin";

        public static IEnumerable<IPlugin> GetAvailablePlugins(string pluginPath)
        {
            // TODO Try Catch
            var pluginPathList = Directory.GetFiles(pluginPath, "*.dll", SearchOption.TopDirectoryOnly);

            return pluginPathList.Select(fileName =>
            {
                if (TryToLoadAssembly(fileName, out Assembly assembly))
                    if (TryToLoadPlugin(assembly, out IPlugin plugin))
                        return plugin;
                return null;
            }).Where(plugin => plugin != null);
        }

        private static bool TryToLoadAssembly(string fileName, out Assembly assembly)
        {
            assembly = null;

            try
            {
                assembly = Assembly.LoadFrom(fileName);
            }
            catch (Exception)
            {
                // TODO запись в журнал, в папке плагинов лежит некорректный плагин
            }

            return (assembly != null);
        }

        private static bool TryToLoadPlugin(Assembly assembly, out IPlugin plugin)
        {
            plugin = null;
            Type[] typeArray = null;

            try
            {
                typeArray = assembly?.GetTypes();
            }
            catch(ReflectionTypeLoadException)
            {
                // TODO запись в лог, проблемная сборка
            }
            if (typeArray == null)
                return false;

            // TODO предполагаем, что в сборке может быть только 1 плагин
            foreach (var type in typeArray)
            {
                if ((!type.IsPublic) || (type.IsAbstract))
                    continue;

                if (type.GetInterface(PluginInterfaceName, false) == null)
                    continue;

                try
                {
                    plugin = assembly.CreateInstance(type.FullName) as IPlugin;
                }
                catch (Exception)
                {
                    // TODO запись в лог, не смогли создать экземпляр IPlugin
                }
            }

            return (plugin != null);
        }
    }
}
