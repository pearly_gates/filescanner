﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using FileScanner.Common;

namespace FileScanner
{
    public class FileSearchEngine : IDisposable
    {
        private bool disposedValue = false;

        private IPlugin _plugin;
        private string _targetFolder;
        private bool _includeSubfolders;

        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _token;

        public FileSearchEngine(string targetFolder, bool includeSubfolders, IPlugin plugin)
        {
            _targetFolder = targetFolder;
            _includeSubfolders = includeSubfolders;
            _plugin = plugin;
        }

        public void Start(Action<string> onNewFile, Action<Task> onFinish)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _token = _cancellationTokenSource.Token;

            var task = Task.Run(() =>
            {
                StartInternal(onNewFile, _token);
            }, _token)
            .ContinueWith(onFinish, _token);
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }

        private void StartInternal(Action<string> onNewFoundFile, CancellationToken token)
        {
            var mode = _includeSubfolders ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            // TODO 63000 файлов читается секунд 30, для оптимизации имеет смысл читать по каталогам
            string[] allFiles = Directory.GetFiles(_targetFolder, "*", mode);
            foreach (var file in allFiles)
            {
                // для имитации долгого поиска
                //Thread.Sleep(1000);
                if (token.IsCancellationRequested)
                    break;

                if (_plugin.CheckFile(file, token))
                    onNewFoundFile(file);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _cancellationTokenSource.Dispose();
                }

                disposedValue = true;
            }
        }
        void IDisposable.Dispose()
        {
            Dispose(true);
        }
    }
}