﻿using System.Threading;
using System.Windows.Forms;

namespace FileScanner.Common
{
    /// <summary>
    /// Класс для общей логики плагинов, пока не пригодился
    /// </summary>
    public abstract class PluginBase : IPlugin
    {
        public abstract string Name { get; }
        public abstract IPluginOptions Options { get; }

        public abstract Form CreateOptionsWindow();
        public abstract bool CheckFile(string file, CancellationToken token);
    }
}
