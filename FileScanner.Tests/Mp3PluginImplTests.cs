﻿using FileScanner.Mp3Plugin;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileScanner.Tests
{
    [TestClass]
    public class Mp3PluginImplTests
    {
        private string _fileName = "After Midnight.mp3";

        [TestMethod]
        public void Mp3PluginImpl_CheckFilePositiveByArtist()
        {
            var plugin = new MP3PluginImpl();
            (plugin.Options as PluginOptions).Artist = "Eric Clapton";
            var result = plugin.CheckFile(_fileName, new System.Threading.CancellationToken());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Mp3PluginImpl_CheckFilePositiveByTitle()
        {
            var plugin = new MP3PluginImpl();
            (plugin.Options as PluginOptions).Title = "After Midnight";
            var result = plugin.CheckFile(_fileName, new System.Threading.CancellationToken());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Mp3PluginImpl_CheckFilePositiveByArtistAndTitle()
        {
            var plugin = new MP3PluginImpl();
            (plugin.Options as PluginOptions).Artist = "Eric Clapton";
            (plugin.Options as PluginOptions).Title = "After Midnight";
            var result = plugin.CheckFile(_fileName, new System.Threading.CancellationToken());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PluginOptions_Mp3PluginImpl_CheckFileNegative()
        {
            var plugin = new MP3PluginImpl();
            (plugin.Options as PluginOptions).Artist = "Eric";
            (plugin.Options as PluginOptions).Title = "After Midnight";
            var result = plugin.CheckFile(_fileName, new System.Threading.CancellationToken());
            Assert.IsFalse(result);
        }
    }
}
