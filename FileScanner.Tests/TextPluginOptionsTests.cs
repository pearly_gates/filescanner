﻿using FileScanner.TextPlugin;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileScanner.Tests
{
    [TestClass]
    public class TextPluginOptionsTests
    {
        [TestMethod]
        public void PluginOptions_NotEmpty()
        {
            var options = new PluginOptions();
            options.TextPattern = "123";

            Assert.AreEqual("123", options.TextPattern);
            Assert.AreEqual(false, options.IsEmpty);
        }

        [TestMethod]
        public void PluginOptions_Empty()
        {
            var options = new PluginOptions();
            options.TextPattern = "";

            Assert.AreEqual("", options.TextPattern);
            Assert.AreEqual(true, options.IsEmpty);
        }
    }
}
