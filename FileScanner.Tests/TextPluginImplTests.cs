﻿using FileScanner.TextPlugin;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileScanner.Tests
{
    [TestClass]
    public class TextPluginImplTests
    {
        private string _fileName = "Hotel California.txt";

        [TestMethod]
        public void TextPluginImpl_CheckFilePositive()
        {
            var plugin = new TextPluginImpl();
            (plugin.Options as PluginOptions).TextPattern = "since 1969";

            var result = plugin.CheckFile(_fileName, new System.Threading.CancellationToken());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TextPluginImpl_CheckFileNegative()
        {
            var plugin = new TextPluginImpl();
            (plugin.Options as PluginOptions).TextPattern = "since  ";

            var result = plugin.CheckFile(_fileName, new System.Threading.CancellationToken());
            Assert.IsFalse(result);
        }
    }
}
